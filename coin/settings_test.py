from settings_base import *

# settings for unit tests

EXTRA_INSTALLED_APPS = (
    'hardware_provisioning',
    'maillists',
    'vpn',
    'vps',
    'housing',
)

TEMPLATE_DIRS = EXTRA_TEMPLATE_DIRS + TEMPLATE_DIRS
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS
